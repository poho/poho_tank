Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount ActionCable.server => '/cable'
  get '/' => 'application#home',      as: :home
  get '/:game_type/' => 'application#play',  as: :play
  # get '/tank/' => 'tank#play',  as: :play
end
