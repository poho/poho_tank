_lobby = {

  startup: function(){
    _lobby.channel.joinChannel();
  },
  receiveMessage: function(raw){
    $('table.lobby').html('');
    $.each(raw.message, function(k,v){
      var row = $($("table.lobby_line").html())
      row.find("td.game_type").html(v.game_type);
      row.find("td.player_count").html(v.player_count);
      var game_id = k.replace("game_","");
      var url = "/"+v.game_type+"/?game="+game_id;
      row.find("td.join a.join_game").attr("href", url);
      $('table.lobby').append(row.html());
    });
  },

  channel: {
    connection: null,

    joinChannel: function(callback){
      _lobby.channel.connection = App.cable.subscriptions.create(
      {
        channel: "GameChannel",
        game_id: 'lobby',
      },
      {
        connected: function() {
          if(callback != undefined){
            callback.call();
          }
        },
        disconnected: function() {
          _pt.sendDisconnect();
        },
        received: function(data) {
          _lobby.receiveMessage(data);
        },
        communicate: function(msg) {
          return this.perform('communicate', msg);
        }
      });
    },
    communicate: function(msg){
      var to_send = {
        channel: 'lobby',
        message: msg
      };
      _lobby.channel.connection.communicate(to_send);
    }
  }
}