_pt.game_channel = {
  currentGameId: null,
  connection: null,

  joinChannel: function(game_id, game_type, callback){
    _pt.game_channel.currentGameId = game_id;
    _pt.game_channel.connection = App.cable.subscriptions.create(
    {
      channel: "GameChannel",
      game_id: game_id,
      game_type: game_type
    },
    {
      connected: function() {
        if(callback != undefined){
          callback.call();
        }
      },
      disconnected: function() {
        console.log("disconnect!");
        _pt.sendDisconnect();
      },
      received: function(data) {
        // console.log(data);
        _pt.receiveMessage(data);
      },
      communicate: function(msg) {
        return this.perform('communicate', msg);
      }
    });
  },
  communicate: function(msg){
    msg._tankId = _pt.myTankId;
    var to_send = {
      channel: _pt.game_channel.currentGameId,
      tankId: _pt.myTankId,
      message: msg
    };
    _pt.game_channel.connection.communicate(to_send);
  }
}