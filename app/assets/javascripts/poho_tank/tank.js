_pt.Tank = function(_name, _id, _main_player){
  var id   = _id;
  var name = _name;

  var main_player = _main_player;
  if(main_player == undefined){
    main_player = false;
  }

  var moveStep  = 0.5;
  var maxHealth = 5;
  var health    = maxHealth;

  var arrowDirection = 0;
  var arrowSize      = 0;
  var canShoot       = true;
  var alive          = true;
  var ready          = false;

  var shootTimeout = 2000;
  if(_pt.dev){
    shootTimeout = 500;
    moveStep = 3;
  }

  var shape = new createjs.Shape();
  shape.graphics.beginStroke("black");
  if(main_player){
    shape.graphics.beginFill("blue");
  }else{
    shape.graphics.beginFill("red");
  }
  shape.graphics.moveTo(-20,10);
  shape.graphics.lineTo(20,10);
  shape.graphics.lineTo(12,-10);
  shape.graphics.lineTo(-12,-10);
  shape.graphics.lineTo(-20,10);
  shape.graphics.closePath();
  shape.x     = 512;
  shape.y     = 100;
  shape.alpha = 0.5;

  var nameString = name + ' ('+health+'/'+maxHealth+')';
  var text = new createjs.Text(nameString, "16px Arial", "red");
  text.textAlign = "center";
  if(main_player){
    text.color = "blue";
  }

  var arrow = null;
  if(main_player){
    arrow = new createjs.Shape();
    arrow.graphics.beginStroke("black");
    arrow.graphics.beginFill("lightpink");
    arrow.graphics.moveTo(0, 0);
    arrow.graphics.lineTo(-5, 0);
    arrow.graphics.lineTo(-5, -10);
    arrow.graphics.lineTo(-10, -10);
    arrow.graphics.lineTo(0, -15);
    arrow.graphics.lineTo(10, -10);
    arrow.graphics.lineTo(5, -10);
    arrow.graphics.lineTo(5, 0);
    arrow.graphics.lineTo(0, 0);
    arrow.graphics.closePath();
    arrow.x = shape.x;
    arrow.scaleY = 3;
  }

  var functions = {
    setX: function(new_x){
      if(main_player){
        arrow.x    = new_x;
        shape.x    = new_x;
        text.x     = new_x;
      }else{
        // createjs.Tween.get(shape).to({x: new_x}, 500, createjs.Ease.linear);
        // createjs.Tween.get(text).to({x: new_x},  500, createjs.Ease.linear);
        shape.x = new_x;
        text.x  = new_x;
      }
    },
    setY: function(new_y){
      if(main_player){
        shape.y    = new_y;
        text.y     = new_y + 30;
        arrow.y    = new_y - 20;
      }else{
        // createjs.Tween.get(shape).to({y: new_y}, 500, createjs.Ease.linear);
        // createjs.Tween.get(text).to({y: new_y+30},  500, createjs.Ease.linear);
        shape.y = new_y;
        text.y  = new_y + 30;
      }
    },
    setHealth: function(new_health){
      this.health = new_health;
      if(this.health == 0){
        this.alive = false;
        shape.alpha = 0.5;
      }
      this.nameChanged();
    },
    nameChanged: function(){
      this.text.text = this.name + '('+this.health+'/'+maxHealth+')';
    },
    notifyShoot: function(){
      this.canShoot    = false;
      this.arrow.graphics._fill.style = "lightpink";

      setTimeout(function(tank){
        tank.canShoot    = true;
        tank.arrow.graphics._fill.style = "green";
      }, shootTimeout, this);  //need to pass myself into the callback

    },
    beenHit: function(){
      if(this.health > 0){
        this.setHealth(this.health-1);
      }
    },
    moveLeft: function(){
      this.setX(shape.x - moveStep);
    },
    moveRight: function(){
      this.setX(shape.x + moveStep);
    },
    arrowLeft: function(){
      if(arrow.rotation > -90){
        arrow.rotation    -= 1;
      }
    },
    arrowRight: function(){
      if(arrow.rotation < 90){
        arrow.rotation    += 1;
      }
    },
    arrowUp: function(){
      if(arrow.scaleY < 8){
        arrow.scaleY    += 0.1;
      }
    },
    arrowDown: function(){
      if(arrow.scaleY > 3){
        arrow.scaleY    -= 0.1;
      }
    },
    setReady: function(val){
      this.ready = val;
      if(this.ready){
        shape.alpha = 1.0;
      }else{
        shape.alpha = 0.5;
      }
    },

    serialize: function(){
      return({
        id:     id,
        name:   this.name,
        x:      this.shape.x,
        health: this.health,
        ready:  this.ready
      });
    },
    shape:    shape,
    arrow:    arrow,
    text:     text,
    health:   health,
    canShoot: canShoot,
    name:     name,
    alive:    alive,
    ready:    ready
  }

  return functions;
}