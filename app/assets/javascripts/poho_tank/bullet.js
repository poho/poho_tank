_pt.Bullet = function(_x, _y, _power, _angle){
  var xSpeed = 1;
  var ySpeed = 1;
  var power  = _power;
  var angle  = _angle;
  var dir    = 1;

  if(angle > 0){
    dir    = 1;
    xSpeed = Math.sin(angle * Math.PI/180);// * pow;
    ySpeed = Math.cos(angle * Math.PI/180);// * pow;
  }
  else if(angle < 0){
    dir    = -1
    // angle  *= -1;
    angle  = 90 - angle;
    xSpeed = Math.cos(angle * Math.PI/180);//*pow;
    ySpeed = Math.sin(angle * Math.PI/180);//*pow;
  }
  else if(angle == 0){
    dir    = 0;
    xSpeed = 0;
    ySpeed = 1;//*this.pow;
  }

  xSpeed *= (power/2);
  ySpeed *= (power/10);
  ySpeed *= -1;

  // console.log("xs: "+ xSpeed +", ys:"+ySpeed+", pwr: "+power+", ang: "+angle);

  var shape = new createjs.Shape();
  shape.graphics.beginFill("Red").drawCircle(0, 0, 5);
  shape.x = _x;
  shape.y = _y;

  var functions = {
    update: function(){
      for(i=0; i<8; i++){
        ySpeed += 0.01;
        shape.x += (xSpeed/5);
        shape.y += (ySpeed/5);
      }
      // console.log(""+shape.x+"," +shape.y + " xspeed: "+ xSpeed +", ySpeed:"+ySpeed);
    },
    serialize: function(){
      return({
        x: x,
        y: y,
        power: power,
        angle: angle
      });
    },
    shape: shape
  };

  return functions;
}