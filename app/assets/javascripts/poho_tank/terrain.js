_pt.terrain = {
  segmentWidth: 80,
  heightVariation: 50,
  terrainStart: 400,
  heightMap: [],
  polygon: null,
  transforms: [],

  setHeightMap: function(hm){
    _pt.terrain.heightMap = hm;
    _pt.terrain.generateTransforms();
    _pt.terrain.createPolygon();
  },

  generate: function(){
    var x = 0;

    _pt.terrain.heightMap = [];
    _pt.terrain.heightMap.push(_pt.terrain.terrainStart);

    while(x < _pt.canvas.width()){
      x += _pt.terrain.segmentWidth;
      var newPos = _pt.terrain.terrainStart + (_pt.terrain.heightVariation - Math.floor(Math.random() * (_pt.terrain.heightVariation * 2)));
      _pt.terrain.heightMap.push(newPos);
    }
    _pt.terrain.generateTransforms();
    _pt.terrain.createPolygon();
  },

  generateTransforms: function(){
    _pt.terrain.transforms = [];
    for(i=0; i < _pt.canvas.width(); i++){
      _pt.terrain.transforms.push(_pt.terrain.generateTransformForX(i));
    }
  },

  getTransform: function(x){
    return(_pt.terrain.transforms[parseInt(x)]);
  },

  generateTransformForX: function(x){
    var segment = Math.floor(x / _pt.terrain.segmentWidth);

    var ret = {};

    var pre = _pt.terrain.heightMap[segment];
    var post = _pt.terrain.heightMap[segment+1];

    var opposite = _pt.terrain.segmentWidth;
    var adjacent = post - pre;

    var flip = false;
    if(adjacent < 0){
      adjacent *= -1;
      flip = true;
    }
    var theta = Math.atan(opposite/adjacent);

    if(flip){
      ret.rotation = ((180 / Math.PI) * theta) - 90;
    }else{
      ret.rotation =  90 - ((180 / Math.PI) * theta);
    }

    opposite = Math.floor(x % _pt.terrain.segmentWidth);

    var height = opposite / Math.tan(theta);

    if(flip){
      ret.y = pre - height;
    }else{
      ret.y = pre + height;
    }
    // _pt.terrain.tankTransforms[x] = ret;
    // return(_pt.terrain.tankTransforms[x]);
    return(ret);
  },

  createPolygon: function(stage){
    if(_pt.terrain.heightMap.length == 0){
      // console.log("No heightmap")
      return;
    }
    _pt.terrain.polygon = new createjs.Shape()

    _pt.terrain.polygon.graphics.beginStroke("black");
    _pt.terrain.polygon.graphics.beginFill("peru");

    var x = 0;
    var y = _pt.terrain.heightMap[0];

    var startX = x;
    var startY = y;

    _pt.terrain.polygon.graphics.moveTo(x, y);

    for(i=1; i<_pt.terrain.heightMap.length; i++){
      x += _pt.terrain.segmentWidth;
      y = _pt.terrain.heightMap[i];
      _pt.terrain.polygon.graphics.lineTo(x, y);
    }

    _pt.terrain.polygon.graphics.lineTo(x, _pt.canvas.height());
    _pt.terrain.polygon.graphics.lineTo(0, _pt.canvas.height());
    _pt.terrain.polygon.graphics.lineTo(startX, startY);

    _pt.terrain.polygon.graphics.closePath();

    return(_pt.terrain);
  },

  serialize: function(){
    return({
      heightMap: _pt.terrain.heightMap
    });
  }
}