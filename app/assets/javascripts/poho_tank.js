_pt = {
  canvas: null,
  context: null,
  stage: null,
  myTankId: (new Date().getMilliseconds()) + Math.floor(Math.random() * 99999999),
  myTank: null, //convenience method
  tanks: {},
  keys: [],

  bullets: {},
  bulletCount: 0,

  //change the FPS at your peril. Thing's arent scaled to this automatically.
  FPS: 30,

  gameState: 'none',

  gameComplete: false,
  toggleReadyAllowed: true,

  displayedText: null,
  subText: null,

  dev: false,
  gameType: 'tank',

  startup: function(){
    _pt.dev = document.location.host == "localhost:3000";

    _pt.canvas  = $('canvas#game_canvas');
    _pt.stage = new createjs.Stage("game_canvas");

    _pt.displayedText = new createjs.Text('', "30px Arial", "Blue");
    _pt.displayedText.textAlign = "center";
    _pt.displayedText.x = _pt.canvas.width() / 2;
    _pt.displayedText.y = 100;
    _pt.stage.addChild(_pt.displayedText);

    _pt.subText = new createjs.Text('', "16px Arial", "Blue");
    _pt.subText.textAlign = "center";
    _pt.subText.x = _pt.canvas.width() / 2;
    _pt.subText.y = 150;
    _pt.stage.addChild(_pt.subText);

    var tmpName = "poho" + parseInt((Math.random() * 999));
    if(_pt.util.getCookie("name") == ""){
      var name = window.prompt("Please enter your name",'');
      _pt.util.setCookie("name", name);
      $("input.name").val(name);
    }else{
      $("input.name").val(_pt.util.getCookie("name"));
    }

    $("input.name").keyup(function(){
      var val = $("input.name").val();
      _pt.util.setCookie("name", val);
      if(_pt.gameState == "in_progress"){
        _pt.myTank.name = val;
        _pt.myTank.nameChanged();
        _pt.broadcastMyTank();
      }
    });

    $("a.new_game").click(function(event){
      event.preventDefault();
      var gameType = $(this).attr('data-game-type');
      _pt.newGame(gameType);
    });

    $("a.join_game").click(function(event){
      event.preventDefault();
      document.location = "/?game="+$('input.join_game').val();
    })

    var gameParam = _pt.util.getQueryParams().game;
    if(gameParam == undefined){
      _pt.newGame();
    }else{
      _pt.joinGame(gameParam);
    }

    window.onkeyup = function(e) {
      _pt.keys[e.keyCode] = false;
    }
    window.onkeydown = function(e) {
      if(e.target == $("input.name")[0] || e.target == $("input.join_game")[0]){
        return;
      }

      if([37,38,39,40,32, 65, 68].indexOf(e.keyCode) != -1){
        e.preventDefault();
      }
      _pt.keys[e.keyCode] = true;
    }

    window.onbeforeunload = function() {
      _pt.disconnect();
    };
  },

  newGame: function(_game_type){
    var game_type = _game_type;
    if(game_type == undefined){
      game_type = 'tank';
    }

    var str = (new Date().getMilliseconds()) + Math.floor(Math.random() * 99999999)
    str = btoa(str).replace("=",'').replace("=",'');
    str = str.substr(0,4);

    if(_pt.gameComplete){
      _pt.game_channel.communicate({t: 'new_game', data: {target: str, game_type: game_type}});
    }

    window.location = window.location.origin + "/tank/?game="+str;
  },

  setGameState: function(new_state){
    _pt.gameState = new_state;
    if(_pt.gameState == "waiting_for_players"){
      _pt.displayedText.text = "Waiting for more players."
    }
  },

  joinGame: function(gameId){
    _pt.addMyTank();
    _pt.setGameState("waiting_for_players")

    _pt.game_channel.joinChannel(gameId, _pt.gameType, function(){
      _pt.game_channel.communicate({t: 'join', gt: _pt.gameType, data: _pt.myTank.serialize()});
    });

    createjs.Ticker.setFPS(_pt.FPS);
    createjs.Ticker.addEventListener("tick", _pt.update);
  },

  createGame: function(){
    _pt.terrain.generate();
    _pt.stage.addChildAt(_pt.terrain.polygon, 0);
    _pt.addTankObjectsToStage(_pt.myTank);
  },

  updateTanks: function(){
    var tankCount = Object.keys(_pt.tanks).length;
    var opponentsAlive = Object.keys(_pt.tanks).length - 1;
    var tanksAlive = 0;
    $.each(_pt.tanks, function(id, tank){
      if(tank.alive){
        tanksAlive += 1;
      }
      var transform = _pt.terrain.getTransform(tank.shape.x);
      if(transform != undefined){
        //in case transforms havent been generated yet coz we are joining a game
        tank.shape.rotation = transform.rotation;
        tank.setY(transform.y - 10);
      }
      if(tank.alive == false && tank != _pt.myTank){
        opponentsAlive -= 1;
      }
    });
    if(_pt.gameState == "in_progress"){
      if(_pt.myTank.alive == false){
        _pt.youLose();
      }
      if(tankCount > 1 && opponentsAlive == 0){
        _pt.youWin();
      }
    }
    if(((tanksAlive == 0 && _pt.dev) || (tanksAlive == 1 && tankCount > 1)) && _pt.gameComplete == false){
      _pt.gameComplete = true;
      _pt.subText.text = "Press 'new game'";
    }
  },

  updateBullets: function(){
    var indexesToRemove = [];
    $.each(_pt.bullets, function(key, bullet){
      bullet.update();

      var hb = 20;
      var bx = bullet.shape.x;
      var by = bullet.shape.y;
      var tx = _pt.myTank.shape.x;
      var ty = _pt.myTank.shape.y;

      // console.log("b: "+bx+","+by+" t: "+tx+","+ty);
      var cleanup = false;

      if(bx > (tx-hb) && bx < (tx + hb)){
        if(by > (ty-hb) && by < (ty + hb)){
           _pt.tankExplode(bx, by);
          setTimeout(function(){_pt.tankExplode(tx,ty)}, 250);
          _pt.myTank.beenHit();
          _pt.broadcastMyTank();
          _pt.game_channel.communicate({t: 'explode', data: {x: bx, y: by}});
          cleanup = true;
        }
      }

      if(bullet.shape.x >= 0 && bullet.shape.x <= _pt.canvas.width()){
        var terrainHeight      = _pt.terrain.getTransform(bullet.shape.x).y;
        var intersectedTerrain = (bullet.shape.y > terrainHeight);
        if(intersectedTerrain){
          _pt.terrainExplode(bx, terrainHeight);
          cleanup = true;
        }
      }else{
        cleanup = true;
      }

      if(cleanup){
        _pt.stage.removeChild(bullet.shape);
        delete _pt.bullets[key];
      }
    });
  },

  update: function(){

    _pt.updateBullets();
    _pt.updateTanks();
    _pt.handleKeys();
    _pt.stage.update();
    // setTimeout(_pt.update, 10);
  },

  handleKeys: function(){
    var tankDirty = false;

    if(_pt.myTank.alive){

      if(_pt.gameState == "in_progress"){
        if(_pt.keys[37]){ //Left Arrow
          _pt.myTank.arrowLeft();
        }
        if(_pt.keys[39]){ //Right Arrow
          _pt.myTank.arrowRight();
        }
         if(_pt.keys[38]){ //UP
          _pt.myTank.arrowUp();
        }
        if(_pt.keys[40]){ //DOWN
          _pt.myTank.arrowDown();
        }

        if(_pt.keys[65]){ //A
          _pt.myTank.moveLeft();
          tankDirty = true;
        }
        if(_pt.keys[68]){ //D
          _pt.myTank.moveRight();
          tankDirty = true;
        }
        if(_pt.keys[32]){ //Space
          _pt.shoot();
        }

        if(tankDirty){
          var rounded = Math.round( _pt.myTank.shape.x * 10) % 10;
          // console.log(rounded);
          //Only send my tank updates if i am on .0 or .5 X co-ordinate
          if(rounded == 0){
            _pt.broadcastMyTank();
          }
        }
      }

      if(_pt.gameState == "waiting_for_players"){
        if(_pt.keys[32]){ //Space
          _pt.toggleReady();
        }
      }

    }

  },

  toggleReady: function(){
    if(false == _pt.toggleReadyAllowed){
      return;
    }

    if(Object.keys(_pt.tanks).length > 1 || _pt.dev){

      _pt.toggleReadyAllowed = false;
      setTimeout(function(){
        _pt.toggleReadyAllowed = true;
      }, 500);

      _pt.myTank.setReady(!_pt.myTank.ready);

      if(_pt.myTank.ready){
        _pt.displayedText.text = "Waiting on others to become ready."
      }else{
        _pt.displayedText.text = "Press [space] to become ready."
      }
      _pt.broadcastMyTank();
      //check all others are ready
      _pt.checkIfGameCanStart();
    }
  },

  checkIfGameCanStart: function(){
    var everyoneReady = true;
    $.each(_pt.tanks, function(id, tank){
      if(false == tank.ready){
        everyoneReady = false;
      }
    });
    if(everyoneReady){
      _pt.startGame();
    }
  },

  startGame: function(){
    _pt.setGameState("in_progress")
    _pt.myTank.notifyShoot();
    _pt.displayedText.text  = "Game has begun!";
    setTimeout(function(){
      _pt.displayedText.text = "";
    }, 2000);
  },

  tankExplode: function(x,y){
    _pt.explode(x,y, ['red','black'], 100);
  },
  terrainExplode: function(x,y){
    _pt.explode(x,y, ['chocolate','peru','sienna','tan'], 20);
  },

  explode: function(x,y, colours, max_travel){
    for(i=0; i < 50; i++){
      var maxTravel = max_travel;
      if(maxTravel == undefined){
        MaxTravel = 100;
      }
      var s = new createjs.Shape();
      // var colours = ["red","black"];
      var colour = colours[parseInt(Math.random() * colours.length)]
      s.graphics.beginFill(colour).drawCircle(x, y, 2);
      _pt.stage.addChild(s);
      var toX = maxTravel - (Math.random() * (maxTravel * 2));
      var toY = maxTravel - (Math.random() * (maxTravel * 2));
      createjs.Tween.get(s).wait(Math.random() * 100).
        to({x: toX, y: toY, alpha: 0},  (200 + (Math.random() * 400)), createjs.Ease.linear).
        call(function(){_pt.stage.removeChild(this)});
    }
  },

  shoot: function(){
    if(_pt.myTank.canShoot == true){
      _pt.myTank.notifyShoot();
      var x = _pt.myTank.shape.x;
      var y = _pt.myTank.shape.y - 20; //-20 so we dont shoot ourselves.
      var p = 2 + (_pt.myTank.arrow.scaleY * 8);
      var a = (_pt.myTank.arrow.rotation / 4);
      _pt.spawnBullet(x,y,p,a);
      var data = {
        x: x,
        y: y,
        p: p,
        a: a
      }
      _pt.game_channel.communicate({t: 'bullet', data: data});
    }
  },

  spawnBullet: function(x,y,p,a){
    var b = new _pt.Bullet(x,y,p,a);
    _pt.bullets[_pt.bulletCount++] = b;
    _pt.stage.addChild(b.shape);
    // _pt.bulletCount += 1;
  },

  addMyTank: function(){
    _pt.myTank = new _pt.Tank($("input.name").val(), _pt.myTankId, true);
    _pt.myTank.setX(10 + Math.floor((Math.random() * (_pt.canvas.width() - 10))));
    _pt.tanks[_pt.myTankId] = _pt.myTank;
  },

  updateOpponentTank: function(data){
    var tankId   = data.id;
    var tankName = data.name;
    if(_pt.tanks[tankId] == undefined){
      _pt.tanks[tankId] = new _pt.Tank(tankName, tankId, false);
      _pt.addTankObjectsToStage(_pt.tanks[tankId]);
    }
    _pt.tanks[tankId].setX(data.x);
    _pt.tanks[tankId].setHealth(data.health);
    _pt.tanks[tankId].name = data.name;
    if(Object.keys(_pt.tanks).length > 1){
      if(_pt.gameState == 'spectating'){
        _pt.displayedText.text = "Game in progress.";
        _pt.subText.text = "Spectating";
      }else{
        if(_pt.gameState == "waiting_for_players"){
          _pt.displayedText.text = "Press [space] to become ready.";
        }
      }
    }

    if(_pt.gameState == "waiting_for_players"){
      _pt.tanks[tankId].setReady(data.ready);
      _pt.checkIfGameCanStart();
    }
  },

  removeOpponentTank: function(data){
    var tankId = data.id;
    if(_pt.tanks[tankId] != undefined){
      _pt.stage.removeChild(_pt.tanks[tankId].shape);
      _pt.stage.removeChild(_pt.tanks[tankId].text);
      delete _pt.tanks[tankId];
    }
  },

  disconnect: function(){
    if(_pt.myTank){
      _pt.game_channel.communicate({t: 'disconnect', data: _pt.myTank.serialize()});
    }
  },

  addTankObjectsToStage: function(tank){
    _pt.stage.addChild(tank.shape);
    _pt.stage.addChild(tank.text);
    if(tank == _pt.myTank){
      _pt.stage.addChild(tank.arrow);
    }
  },

  receiveMessage: function(raw){
    // console.log(raw.message.t);
    // console.log(raw);
    if(raw.message._tankId == _pt.myTankId){
      if(raw.message.t == 'join' && raw.pc == 1){
        //I have just joined a game and discovered no-one else is here
        //I am receiving my own 'join' broadcast
        _pt.createGame();
      }
      return; //dont process messages we sent
    }

    if(raw.message.t == 'map'){
      if(_pt.terrain.heightMap.length == 0){
        _pt.terrain.setHeightMap(raw.message.data.heightMap);
        _pt.stage.addChildAt(_pt.terrain.polygon, 0);
        if(raw.message.data.game_in_progress){
          _pt.displayedText.text = "Game is already in progress."
          _pt.gameState = 'spectating';
        }else{
          _pt.addTankObjectsToStage(_pt.myTank);
        }
      }
    }

    if(raw.message.t == 'join'){
      //someone else has joined, tell them about us and the map!
      var map_data              = _pt.terrain.serialize();
      //if game is already in progress, let them know
      map_data.game_in_progress = (_pt.gameState == "in_progress");

      _pt.game_channel.communicate({t: 'map',  data: map_data});
      _pt.broadcastMyTank();
      if(_pt.gameState != "in_progress"){
        _pt.updateOpponentTank(raw.message.data);
      }
    }

    if(raw.message.t == 'tank'){
      _pt.updateOpponentTank(raw.message.data);
    }

    if(raw.message.t == 'disconnect'){
      _pt.removeOpponentTank(raw.message.data);
    }

    if(raw.message.t == 'bullet'){
      _pt.spawnBullet(raw.message.data.x, raw.message.data.y, raw.message.data.p, raw.message.data.a);
    }

    if(raw.message.t == 'explode'){
      _pt.tankExplode(raw.message.data.x, raw.message.data.y);
    }

    if(raw.message.t == 'new_game'){
      $("a.new_game").attr('disabled', 'disabled')
      _pt.displayedText.text = "Joining new game...";
      _pt.subText.text = '';
      setTimeout(function(){
        document.location = "/?game=" + raw.message.data.target;
      }, 2000);
    }
  },

  broadcastMyTank: function(){
    _pt.game_channel.communicate({t: 'tank', data: _pt.myTank.serialize()});
  },

  youWin: function(){
    _pt.displayedText.text  = "You Win!";
    _pt.setGameState("game_over");
  },

  youLose: function(){
    _pt.displayedText.text  = "You Lose";
    _pt.setGameState("game_over");
  }

}