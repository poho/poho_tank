# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class GameChannel < ApplicationCable::Channel
  @@games = {}

  def subscribed
    if params['game_id'] == 'lobby'
      stream_from 'lobby'
    else
      channel_name = "game_#{params['game_id']}"
      stream_from channel_name

      @@games[channel_name] ||= {}
      @@games[channel_name][:player_count] ||= 0
      @@games[channel_name][:player_count] += 1
      @@games[channel_name][:game_type] = params['game_type']
    end
    broadcast_lobby
  end

  def communicate(data)
    channel = "game_#{data['channel']}"
    player_count = @@games[channel][:player_count]
    ActionCable.server.broadcast(channel, message: data['message'], pc: player_count)
  end

  def unsubscribed
    # # Any cleanup needed when channel is unsubscribed
    channel_name = "game_#{params['game_id']}"
    @@games[channel_name][:player_count] -= 1
    @@games.delete(channel_name) if @@games[channel_name][:player_count] == 0
    broadcast_lobby
  end

  def broadcast_lobby
    ActionCable.server.broadcast('lobby', message: @@games)
  end

end
