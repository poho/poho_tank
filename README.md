# README

TODO:
* Change tank image when dead (change alpha?)
* Update back end to use gamestate rather than discrete variables
* Make first player generate map if no response in ~1 second to join message
* Implement graphics?
* Make the canvas responsive?
* Make it work on phones


STATES:
* none
* waiting_for_players
* in_progress
* game_over #I am dead but game continues
* game_complete #The game has finished


INSTALLING:
Need mod_proxy, mod_wstunnel
Run the cable script to get the socket going
Add the authorized hosts in config/environments/production.rb
